package com.example.vote;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnTouchListener {

	private ImageView imageView1;
	private ImageView imageView2;
	private ImageView imageView3;
	private ImageView imageView4;
	private Button button;
	private TranslateAnimation upAnimation;
	private TranslateAnimation downAnimation;
	private int animationDuration = 300;
	private int move = 1000;
	private boolean animatonComplete = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		imageView1 = (ImageView) findViewById(R.id.imageView1);
		imageView2 = (ImageView) findViewById(R.id.imageView2);
		imageView3 = (ImageView) findViewById(R.id.imageView3);
		imageView4 = (ImageView) findViewById(R.id.imageView4);
		button = (Button) findViewById(R.id.button1);
		imageView1.setOnTouchListener(this);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imageView2.setImageResource(R.drawable.image2);
				imageView4.setImageResource(R.drawable.image3);
				button.setVisibility(View.INVISIBLE);
				imageView3.setVisibility(View.VISIBLE);
				imageView2.setVisibility(View.VISIBLE);
				imageView4.setVisibility(View.VISIBLE);
				imageView1.setOnTouchListener(MainActivity.this);
			}
		});

	}

	float mLastTouchX;
	float mLastTouchY;
	float mPosX;
	float mPosY;

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		// TODO Auto-generated method stub
		final int action = MotionEventCompat.getActionMasked(event);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:

			upAnimation = new TranslateAnimation(0, 0, 0, -50);
			upAnimation.setDuration(animationDuration);
			upAnimation.setFillAfter(true);
			imageView2.startAnimation(upAnimation);

			downAnimation = new TranslateAnimation(0, 0, 0, 50);
			downAnimation.setDuration(animationDuration);
			downAnimation.setFillAfter(true);
			imageView4.startAnimation(downAnimation);

			final float x = event.getX();
			final float y = event.getY();

			// Remember where we started (for dragging)
			mLastTouchX = x;
			mLastTouchY = y;
			// Save the ID of this pointer (for dragging)
			// mActivePointerId = MotionEventCompat.getPointerId(event, 0);

			break;

		case MotionEvent.ACTION_UP:

			upAnimation = new TranslateAnimation(0, 0, -50, 0);
			upAnimation.setDuration(animationDuration);
			upAnimation.setFillAfter(true);
			imageView2.startAnimation(upAnimation);

			downAnimation = new TranslateAnimation(0, 0, 50, 0);
			downAnimation.setDuration(animationDuration);
			downAnimation.setFillAfter(true);
			imageView4.startAnimation(downAnimation);
			mPosX = 0.0f;
			mPosY = 0.0f;
			imageView4.setAlpha(255);
			imageView2.setAlpha(255);
			if (animatonComplete) {
				imageView1.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						return false;
					}
				});
			}

			break;

		case MotionEvent.ACTION_MOVE: {
			imageView3.setAlpha(50);

			final float x1 = event.getX();
			final float y1 = event.getY();

			// Calculate the distance moved
			final float dx = x1 - mLastTouchX;
			final float dy = y1 - mLastTouchY;

			mPosX += dx;
			mPosY += dy;

			if (mPosY < -200) {
				imageView4.setAlpha(100);
				Log.i("Duration", mPosY + "");
			} else if (mPosY > 200) {
				imageView2.setAlpha(100);
			}
			if (mPosY < -move) {
				imageView2.setImageResource(R.drawable.image7);
				button.setVisibility(View.VISIBLE);
				imageView3.setVisibility(View.INVISIBLE);
				imageView4.setVisibility(View.INVISIBLE);
				animatonComplete = true;
			} else if (mPosY > move) {
				imageView4.setImageResource(R.drawable.image8);
				button.setVisibility(View.VISIBLE);
				imageView3.setVisibility(View.INVISIBLE);
				imageView2.setVisibility(View.INVISIBLE);
				animatonComplete = true;
			}

			break;
		}

		}

		return true;
	}
}
